import java.util.Scanner;

public class Investment {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

      
        System.out.print("Enter the initial investment amount: ");
        double initialInvestment = scanner.nextDouble();

    
        System.out.print("Enter the percentage increase in the first year: ");
        double firstYearPercentage = scanner.nextDouble();
        double firstYearIncrease = initialInvestment * (firstYearPercentage / 100.0);
        double firstYearValue = initialInvestment + firstYearIncrease;

       
        System.out.print("Enter the direct reduction in the second year: ");
        double secondYearReduction = scanner.nextDouble();
        double secondYearValue = firstYearValue - secondYearReduction;

 
        System.out.print("Enter the percentage increase in the third year: ");
        double thirdYearPercentage = scanner.nextDouble();
        double thirdYearIncrease = secondYearValue * (thirdYearPercentage / 100.0);
        double thirdYearValue = secondYearValue + thirdYearIncrease;


        System.out.println("Initial Investment: " + initialInvestment);
        System.out.println("Value after the first year: " + firstYearValue);
        System.out.println("Value after the second year: " + secondYearValue);
        System.out.println("Value after the third year: " + thirdYearValue);

        scanner.close(); // Close the scanner to avoid resource leaks
    }
}
