public class QuadraticFunction {
    private int a;
    private int b;
    private int c;

    // Constructor with no parameters (setting a, b, and c to 1)
    public QuadraticFunction() {
        this(1, 1, 1);
    }

    // Constructor with 3 parameters to specify a, b, and c
    public QuadraticFunction(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Extractor for the value of a b and c
    public int getA() {
        return a;
    }

    
    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    // Modifier to set new values for a, b, and c
    public void setABC(int newA, int newB, int newC) {
        this.a = newA;
        this.b = newB;
        this.c = newC;
    }

    // Compute method to calculate f(x) for the given values of a, b, and c
    public double compute(int x) {
        return (a * x * x) + (b * x) + c;
    }

    public static void main(String[] args) {
        // setting default values for instance quadratic function (a=1, b=1, c=1)
        QuadraticFunction quadraticFunction1 = new QuadraticFunction();
        
        // Create an instance with specified values (a=2, b=3, c=4)
        QuadraticFunction quadraticFunction2 = new QuadraticFunction(2, 3, 4);

        // Display initial values
        System.out.println("Initial values:");
        System.out.println("quadraticFunction1: a=" + quadraticFunction1.getA() +
                ", b=" + quadraticFunction1.getB() +
                ", c=" + quadraticFunction1.getC());
        
        System.out.println("quadraticFunction2: a=" + quadraticFunction2.getA() +
                ", b=" + quadraticFunction2.getB() +
                ", c=" + quadraticFunction2.getC());

        // Modify values
        quadraticFunction1.setABC(29, 9, 4);
        quadraticFunction2.setABC(2, 1, 3);

       
        System.out.println("\nModified values:");
        System.out.println("quadraticFunction1: a=" + quadraticFunction1.getA() +
                ", b=" + quadraticFunction1.getB() +
                ", c=" + quadraticFunction1.getC());
        
        System.out.println("quadraticFunction2: a=" + quadraticFunction2.getA() +
                ", b=" + quadraticFunction2.getB() +
                ", c=" + quadraticFunction2.getC());

        // f(x) for some random values of x
        int x = 3;
        System.out.println("\nf(" + x + ") for quadraticFunction1: " + quadraticFunction1.compute(x));
        System.out.println("f(" + x + ") for quadraticFunction2: " + quadraticFunction2.compute(x));
    }
}
