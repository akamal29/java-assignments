import java.util.*;
public class NumberMatrix10 {

    public static void main(String[] args) {
        
        int rows = 4;

  
        int[][] matrix = new int[rows][];

        
        int counter = 1;
        for (int i = 0; i < rows; i++) {
            matrix[i] = new int[i + 1];
            for (int j = 0; j <= i; j++) {
                matrix[i][j] = counter++;
            }
        }

        
        displayMatrix(matrix);
    }

    private static void displayMatrix(int[][] matrix) {
        for (int[] row : matrix) {
            for (int num : row) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
