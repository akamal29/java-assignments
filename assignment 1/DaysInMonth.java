import java.util.Scanner;
public class DaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the month (1 to 12): ");
        int month = scanner.nextInt();

        System.out.print("Enter the year: ");
        int year = scanner.nextInt();

        if (isValidInput(month, year)) {
            int daysInMonth = calculateDaysInMonth(month, year);
            System.out.println("Number of days in the month: " + daysInMonth);
        } else {
            System.out.println("Invalid input. Please enter a valid month and year.");
        }
    }

    public static boolean isValidInput(int month, int year) {
        return (month >= 1 && month <= 12 && year >= 1);
    }
    public static int calculateDaysInMonth(int month, int year) {
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12:
                return 31;
            case 4, 6, 9, 11:
                return 30;
            case 2:
                return isLeapYear(year) ? 29 : 28;
            default:
                return -1; // Invalid month
        }
    }

    public static boolean isLeapYear(int year) {
        // Leap year logic: divisible by 4 but not divisible by 100 unless divisible by 400
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
}





//import java.util.*;
//public class DaysInMonth {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Enter the month (1 to 12): ");
//        int month = scanner.nextInt();
//
//        System.out.print("Enter the year: ");
//        int year = scanner.nextInt();
//
//        if (month < 1 || month > 12 || year < 1) {
//            System.out.println("Invalid input. Please enter a valid month and year.");
//        } else {
//            
//            int daysInMonth = calculateDaysInMonth(month, year);
//            System.out.println("Number of days in the month: " + daysInMonth);
//        }
//
//        scanner.close();
//    }
//    public static int calculateDaysInMonth(int month, int year) {
//        switch (month) {
//            case 1:
//            case 3: 
//            case 5: 
//            case 7:
//            case 8: 
//            case 10:
//            case 12:
//                return 31;
//            case 4: 
//            case 6: 
//            case 9: 
//            case 11: 
//                return 30;
//            case 2: 
//                return isLeapYear(year) ? 29 : 28;
//            default:
//                return -1; // Invalid month
//        }
//    }
//    public static boolean isLeapYear(int year) {
//        //leap year logic
//        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
//    }
//}
