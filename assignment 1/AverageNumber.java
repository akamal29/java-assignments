import java.util.*;
//Main class gets input and passes input to average calculator method
public class AverageNumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of elements: ");
        int n = scanner.nextInt();
       
        double[] numbers = new double[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            numbers[i] = scanner.nextDouble();
        }

        double average = calculateAverage(numbers);
        System.out.println("Average: " + average);

        scanner.close();
    }

  //Method that contains logic to calculate average of n numbers.
    private static double calculateAverage(double[] arr) {
        if (arr.length == 0) {
            return 0; 
        }

        double sum = 0;
        for (double num : arr) {
            sum += num;
        }

        return sum / arr.length;
    }
}
