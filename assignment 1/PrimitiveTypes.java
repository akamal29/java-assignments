import java.util.*;
public class PrimitiveTypes{
    public static void main(String[] args) {
        byte byteVar = 100; 
        byteVar = (byte) (byteVar + 1); // Explicit casting from int to byte
        
        short shortVar = 300;
        shortVar = (short) (shortVar + 1);
        
        int intVar = 23456787;  
        intVar = intVar + 1; // No explicit casting needed

        long longVar = 9111111111111111111L; 
        longVar = longVar + 1; // No explicit casting needed for long
      
        float floatVar = 3.14f; 
        floatVar = intVar; // Implicit casting from int to float

        double doubleVar = 2.71828; 
        doubleVar = floatVar; // Implicit casting from float to double

        char charVar = 'A'; 
        int asciiValue = charVar; // Implicit casting from char to int
      
        boolean boolVar = true; // Initializing with a boolean literal
        // casting gives compilation error
        
        
        System.out.println("byteVar: " + byteVar);
        System.out.println("shortVar: " + shortVar);
        System.out.println("intVar: " + intVar);
        System.out.println("longVar: " + longVar);
        System.out.println("floatVar: " + floatVar);
        System.out.println("doubleVar: " + doubleVar);
        System.out.println("charVar: " + charVar + " (ASCII value: " + asciiValue + ")");
        System.out.println("boolVar: " + boolVar);
    }
}
