import java.util.*;

public class ReverseElements {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of elements: ");
        int n = scanner.nextInt();
        
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            numbers[i] = scanner.nextInt();
        }

        //Reversing the array elements logic
        int start = 0;
        int end = n - 1;

        while (start < end) {
           
            int temp = numbers[start];
            numbers[start] = numbers[end];
            numbers[end] = temp;

     
            start++;
            end--;
        }

     
        System.out.println("Reversed Array: ");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
        scanner.close();
    }
}
