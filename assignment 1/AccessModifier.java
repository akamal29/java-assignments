import java.util.*;
class AccessModifierTypes {
   
    public int publicVar = 10;
    private int privateVar = 20;
    protected int protectedVar = 30;
    int defaultVar = 40;

    // Public method
    public void publicMethod() {
        System.out.println("Public method called.");
    }

    private void privateMethod() {
        System.out.println("Statement displayed from private method.");
    }


    protected void protectedMethod() {
        System.out.println("Protected method is invoked.");
    }

    // Default (package-private) method
    void defaultMethod() {
        System.out.println("Default method called.");
    }
}

public class AccessModifier {

    public static void main(String[] args) {
       
        AccessModifierTypes obj = new AccessModifierTypes();

        // Accessing public members
        System.out.println("Public variable: " + obj.publicVar);
        obj.publicMethod();

        // Accessing private members gives (Compilation Error)
      
       

        // Accessing protected members (Compilation Error)
        

        // Accessing default (package-private) members
        System.out.println("Default variable: " + obj.defaultVar);
        obj.defaultMethod();
    }
}
