import java.util.*;

public class LargestElementInRow {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        System.out.print("Enter the number of columns: ");
        int columns = scanner.nextInt();

        
        int[][] array = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position [" + i + "][" + j + "]: ");
                array[i][j] = scanner.nextInt();
            }
        }

      
        findAndPrintLargestInEachRow(array);

      
        scanner.close();
    }

 
    private static void findAndPrintLargestInEachRow(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
        	//assuming first element as largest initially and then iterating from then
            int largestElement = arr[i][0];

            for (int j = 1; j < arr[i].length; j++) {
                if (arr[i][j] > largestElement) {
                    largestElement = arr[i][j];
                }
            }

            System.out.println("Largest element in row " + i + ": " + largestElement);
        }
    }
}
