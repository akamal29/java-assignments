import java.util.*;
class FirstClass {
    
    protected int protectedData = 29;
}


class SecondClass {
   
    void manipulateProtectedData(FirstClass obj) {
        
        obj.protectedData = 9;
        System.out.println("Protected data in SecondClass: " + obj.protectedData);
    }
}

public class ProtectedDataManipulation {
    public static void main(String[] args) {
        
        FirstClass firstObj = new FirstClass();
        SecondClass secondObj = new SecondClass();

     
        System.out.println("Initial protected data in FirstClass: " + firstObj.protectedData);

        secondObj.manipulateProtectedData(firstObj);

        System.out.println("Modified protected data in FirstClass: " + firstObj.protectedData);
    }
}
