import java.util.*;

public class LocationOfElement {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       
        System.out.print("Enter the number of elements: ");
        int n = scanner.nextInt();

        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            numbers[i] = scanner.nextInt();
        }

        System.out.print("Enter the element to find: ");
        int target = scanner.nextInt();

        
        int location = findElementLocation(numbers, target);
        if (location != -1) {
            System.out.println("Element " + target + " found at index " + location);
        } else {
            System.out.println("Element " + target + " not found in the array.");
        }

        
        scanner.close();
    }

    public static int findElementLocation(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) {
                return i; 
            }
        }
        return -1; 
    }
}
