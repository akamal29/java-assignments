import java.util.*;
//Main class gets input and passes to Prime number checking method and prints result
public class PrimeNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

       
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        
        boolean isPrime = isPrimeNumber(number);

        
        if (isPrime) {
            System.out.println(number + " is a prime number.");
        } else {
            System.out.println(number + " is not a prime number.");
        }

        scanner.close();
    }
//Method contains logic to find prime number
    private static boolean isPrimeNumber(int num) {
        if (num <= 1) {
            return false;
        }
//logic-n/2 will contain any of the factors(if present)
        for (int i = 2; i <= num/2; i++) {
            if (num % i == 0) {
                return false; 
            }
        }

        return true; 
    }
}
