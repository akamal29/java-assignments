import java.util.Scanner;

public class NumberToWords {

  
    private static final String[] digitWords = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        String words = convertToWords(number);
        System.out.println("In words: " + words);

        scanner.close();
    }

   
    private static String convertToWords(int num) {
        if (num == 0) {
            return digitWords[0];
        }

        // Convert each digit to words
        StringBuilder result = new StringBuilder();
        while (num > 0) {
            int digit = num % 10;
            result.insert(0, digitWords[digit] + " ");
            num /= 10;
        }

        return result.toString().trim();
    }
}
