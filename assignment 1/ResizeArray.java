import java.util.*;
public class ResizeArray
{
	public static void main(String[] args)
	{
		 int[] a = {29, 01, 2003};
		    //Array cant be resized , hence creating copy of the array
		    a = Arrays.copyOf(a, a.length + 1);
		    for (int i : a)
		        System.out.println(i);
	}
}