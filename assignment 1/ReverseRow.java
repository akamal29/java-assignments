import java.util.*;
public class ReverseRow {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        System.out.print("Enter the number of columns: ");
        int columns = scanner.nextInt();

      
        int[][] array = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position [" + i + "][" + j + "]: ");
                array[i][j] = scanner.nextInt();
            }
        }

        System.out.println("\n 2D array:");
        printArray(array);

        reverseElementsInRows(array);

        System.out.println("\n2D array after reversing elements in each row:");
        printArray(array);


        scanner.close();
    }

 
    public static void reverseElementsInRows(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
        	
        	//using reverse logic for each row array[i]
            int start = 0;
            int end = arr[i].length - 1;

            while (start < end) {
               
                int temp = arr[i][start];
                arr[i][start] = arr[i][end];
                arr[i][end] = temp;

                start++;
                end--;
            }
        }
    }

   
    private static void printArray(int[][] arr) {
        for (int[] row : arr) {
            for (int element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}
