import java.util.*;

public class ElementSearch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter the number of elements in the array: ");
        int n = scanner.nextInt();

        
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            numbers[i] = scanner.nextInt();
        }

        
        System.out.print("Enter the element to search for: ");
        int target = scanner.nextInt();

     
        boolean found = false;

        System.out.print("Element " + target + " found at index: ");
        for (int i = 0; i < n; i++) {
            if (numbers[i] == target) {
                System.out.print(i + " ");
                found = true;
            }
        }

        if (!found) {
            System.out.println("Element " + target + " not found in the array.");
        }

        scanner.close();
    }
}
