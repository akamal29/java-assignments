public class TestProgram {

    final int constantValue = 10;

    static int staticValue = 50;

    private int value; // 

    public TestProgram(int value) {
        this.value = value;
    }

    // Non-static method accessing static member
    void nonStaticMethod() {
        System.out.println("Constant Value in nonStaticMethod: " + constantValue);
        System.out.println("Static Value in nonStaticMethod: " + staticValue);
    }

    public static void main(String[] args) {
        TestProgram obj = new TestProgram(42);

        // Accessing constant member
        System.out.println("Constant Value in main: " + obj.constantValue);

        // Accessing static member in a static method
        System.out.println("Static Value in main: " + staticValue);

        // Accessing constant and static members in a non-static method
        obj.nonStaticMethod();

        // modifying constant member will result in a compilation error
        // obj.constantValue = 20;
    }
}
