import java.util.*;

public class StudentResults {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of students: ");
        int numStudents = scanner.nextInt();

        System.out.print("Enter the number of subjects: ");
        int numSubjects = scanner.nextInt();

        int[][] marksArray = new int[numStudents][numSubjects];

   
        for (int i = 0; i < numStudents; i++) {
            for (int j = 0; j < numSubjects; j++) {
                System.out.print("Enter marks for student " + (i + 1) + " in subject " + (j + 1) + ": ");
                marksArray[i][j] = scanner.nextInt();
            }
        }

        System.out.println("\nStudent Result:");

        for (int i = 0; i < numStudents; i++) {
            int totalMarks = 0;
            for (int j = 0; j < numSubjects; j++) {
                totalMarks += marksArray[i][j];
            }
            
            double averageMarks = (double) totalMarks / numSubjects;

            System.out.println("Total marks for student " + (i + 1) + ": " + totalMarks);
            System.out.println("Average marks for student " + (i + 1) + ": " + averageMarks);
        }

        scanner.close();
    }
}
