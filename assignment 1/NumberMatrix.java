import java.util.*;
public class NumberMatrix {

    public static void main(String[] args) {
      
        int[][] matrix = new int[4][4];//declaring the matrix

        int count = 1;
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                matrix[row][col] = count;
                count++;
            }
        }

        
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.printf("%2d ",matrix[row][col]);
            }
            System.out.println();
        }
    }
}
