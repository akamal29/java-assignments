
class Biriyani {
	//only the types should be known to this biriyani class and not by other class
 private String CityType;
 private String NVType;
 private String SpiceType;
 private String SideDishType;

 public Biriyani(String CityType, String NVType, String SpiceType, String SideDishType) {
     this.CityType = CityType;
     this.NVType = NVType;
     this.SpiceType = SpiceType;
     this.SideDishType = SideDishType;
 }

 
 public String toString() {
     return "Biriyani with " + CityType + " CityType, " + NVType + " NVType, " +
             SpiceType + " SpiceType, and SideDishType: " + SideDishType;
 }
}


interface BiriyaniBuilder {
 BiriyaniBuilder setCityType(String CityType);
 BiriyaniBuilder setNVType(String NVType); 
 BiriyaniBuilder setSpiceType(String SpiceType);
 BiriyaniBuilder setSideDishType(String SideDishType);
 Biriyani build();
}

class CustomBiriyaniBuilder implements BiriyaniBuilder {
 private String CityType;
 private String NVType;
 private String SpiceType;
 private String SideDishType;

 
 public BiriyaniBuilder setCityType(String CityType) {
     this.CityType = CityType;
     return this;
 }


 public BiriyaniBuilder setNVType(String NVType) {
     this.NVType = NVType;
     return this;
 }


 public BiriyaniBuilder setSpiceType(String SpiceType) {
     this.SpiceType = SpiceType;
     return this;
 }

 
 public BiriyaniBuilder setSideDishType(String SideDishType) {
     this.SideDishType = SideDishType;
     return this;
 }

 
 public Biriyani build() {
     return new Biriyani(CityType, NVType, SpiceType, SideDishType);
 }
}

public class BuilderExample {
 public static void main(String[] args) {

     BiriyaniBuilder builder = new CustomBiriyaniBuilder();
     Biriyani biriyani = builder
         .setCityType("Chennai")
         .setNVType("Chicken")
         .setSpiceType("Hot")
         .setSideDishType("Chicken65")
         .build();
System.out.println(biriyani.toString());
 }
}
