// Abstract CPU class
abstract class CPU {
    abstract void process();
}

// Concrete CPU classes
class GamingCPU extends CPU {
    void process() {
        System.out.println("Processing for gaming performance.");
    }
}

class OfficeCPU extends CPU {
    void process() {
        System.out.println("Processing for office tasks.");
    }
}

// Abstract RAM class
abstract class RAM {
    abstract void allocate();
}

// Concrete RAM classes
class GamingRAM extends RAM {
    void allocate() {
        System.out.println("Allocating RAM for gaming applications.");
    }
}

class OfficeRAM extends RAM {
    void allocate() {
        System.out.println("Allocating RAM for office applications.");
    }
}

// Abstract ComputerFactory class
abstract class ComputerFactory {
    abstract CPU createCPU();
    abstract RAM createRAM();
}

// Concrete ComputerFactory classes
class GamingComputerFactory extends ComputerFactory {
    CPU createCPU() {
        return new GamingCPU();
    }

    RAM createRAM() {
        return new GamingRAM();
    }
}

class OfficeComputerFactory extends ComputerFactory {
    CPU createCPU() {
        return new OfficeCPU();
    }

    RAM createRAM() {
        return new OfficeRAM();
    }
}

public class AbstractFactoryExample {
    public static void main(String[] args) {
        ComputerFactory factory = new GamingComputerFactory();

        CPU cpu = factory.createCPU();
        RAM ram = factory.createRAM();

        cpu.process();
        ram.allocate();
    }
}
