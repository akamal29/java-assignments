
abstract class ReportTemplate {

	// Template method (steps)
	final void generateReport() {
		formatHeader();
		generateContent();
		formatFooter();
	}

	// Abstract method- to be altered by subclasses
	abstract void generateContent();

	// Default implementation for formatting the header
	void formatHeader() {
		System.out.println("Formatting the report header.");
	}

	// Default implementation for formatting the footer
	void formatFooter() {
		System.out.println("Formatting the report footer.");
	}
}

//class implementing the template
class PDFReport extends ReportTemplate {

	@Override
	void generateContent() {
		System.out.println("Generating content for a PDF report.");
	}

	// Overriding the template method to customize behavior
	@Override
	void formatFooter() {
		System.out.println("Adding page numbers to the PDF report footer.");
	}
}

// Concrete class implementing the template
class CSVReport extends ReportTemplate {

	@Override
	void generateContent() {
		System.out.println("Generating content for a CSV report.");
	}

	// Overriding the template method to customize behavior
	@Override
	void formatFooter() {
		System.out.println("Adding summary information to the CSV report footer.");
	}
}

// Client code
public class TemplateExample {
	public static void main(String[] args) {
		System.out.println("Generating a PDF Report:");
		ReportTemplate pdfReport = new PDFReport();
		pdfReport.generateReport();

		System.out.println("\nGenerating a CSV Report:");
		ReportTemplate csvReport = new CSVReport();
		csvReport.generateReport();
	}
}