interface Vehicle {
    void start();
}
class Car implements Vehicle {

    public void start() {
        System.out.println("Car started.");
    }
}
class Bike implements Vehicle {

    public void start() {
        System.out.println("Bike started.");
    }
}

public class FactoryExample {
    Vehicle createCar() {
        return new Car();
    }

    Vehicle createBike() {
        return new Bike();
    }

    public static void main(String[] args) {
        FactoryExample vehicleFactory = new FactoryExample();

        Vehicle car = vehicleFactory.createCar();
        car.start();

        Vehicle bike = vehicleFactory.createBike();
        bike.start();
    }
}
