//closed for modification -open for extension
public class StrategyExample{
	public static void main(String[] args) {
		FoodItem item=new FoodItem();
		//food item class is closed for modification and is open for extension
		Spice s=new SpiceB();
		item.s=s;
		item.doSpice();
		}
}
class FoodItem{
	Spice s;//Holds a reference for spice making it easy for modification (like spice a spice b ..)
	public void doSpice() {
		System.out.println(s);
	}
}
abstract class Spice{	
	abstract void PrintMessage();
}
class SpiceA extends Spice{
	public void PrintMessage()
	{
		System.out.println("Class A-Spice A");
	}
	
}
class SpiceB extends Spice{
	public void PrintMessage()
	{
		System.out.println("Class B-Spice B");
	}
	
}
class SpiceC extends Spice{
	public void PrintMessage()
	{
		System.out.println("Class C=Spice B");
	}
}
//n number of spices can be added-can eliminate if else loop