import java.util.Scanner;

public class StateExample {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        DocumentProcessor processor = new DocumentProcessor();
        while (true) {      //infinite loop  
        	System.out.println("Move To next State:");
            sc.next();
            processor.moveToNextAction();
 //Calls the moveToNextAction() method on the DocumentProcessor instance.
            processor.performAction();
//Calls the performAction() method on the DocumentProcessor instance.																																																				
        }
    }
  
}

class DocumentProcessor {
	//used for managing states and transitioning between them
    DocumentState[] states;
    int currentStateIndex;

    public DocumentProcessor() {
        states = new DocumentState[]{new EditState(), new SaveState(), new PrintState()};
        currentStateIndex = 0;
    }

    public void moveToNextAction() {
        currentStateIndex = (currentStateIndex + 1) % states.length;
    }

    public void performAction() {
        states[currentStateIndex].executeAction();
    }
}

abstract class DocumentState {
    abstract void executeAction();
}

class EditState extends DocumentState {

    void executeAction() {
        System.out.println("Editing the document...");
    }
}

class SaveState extends DocumentState {
    
    void executeAction() {
        System.out.println("Saving the document...");
    }
}

class PrintState extends DocumentState {
    
    void executeAction() {
        System.out.println("Printing the document...");
    }
}