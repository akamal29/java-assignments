public class PrototypeExample {
    public static void main(String[] args) throws CloneNotSupportedException {
        //new Student object called "representative"
        Student representative = new Student();

        //  Create a shallow copy of the "representative" object called "lowPerformingStudent"
        Student lowPerformingStudent = representative.createShallowCopy();

        // Modify the "name" property of the "representative" and "lowPerformingStudent" objects
        representative.name = "Representative Student name";
        lowPerformingStudent.name = "Low performing student.(Only resources -name is changed , rest all properties are same)";

        // Print the "name" property of both objects for identification
        System.out.println(representative.name);
        System.out.println(lowPerformingStudent.name);

        // Call the HSCMarksheet method for both objects
        representative.HSCMarksheet();
        lowPerformingStudent.HSCMarksheet();
    }
}

class Student implements Cloneable {
    //property "name"
    String name;  // resources are shared, and properties are unique

    // Constructor for the Student class
    public Student() {
        System.out.println("Student object created.");
    }

    //  Methods for issuing  class 10 and 12th marksheet
    public void HSCMarksheet() {
        System.out.println("Hsc marksheet issued.");
    }

    
    public void AISSEMarksheet() {
        System.out.println("AISSE marksheet issued.");
    }

    // Method to create a shallow copy of the Student object
    public Student createShallowCopy() throws CloneNotSupportedException {//exception thrown when not uses cloneable interface
        return (Student) super.clone();
    }
}
