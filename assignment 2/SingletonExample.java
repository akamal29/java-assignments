public class SingletonExample {
    private static SingletonExample instance;

    private SingletonExample() {
        // No new object can be created outside this class
    }

    public static SingletonExample getInstance() {
        if (instance == null) {
            instance = new SingletonExample();
        }
        return instance;
    }

    public void showMessage() {
        System.out.println("Global access view of objects is covered in singleton");
        System.out.println("Ensures that only one instance of a class is created.");
    }

    public static void main(String[] args) {
        // Create the first instance
        SingletonExample singleton1 = SingletonExample.getInstance();
        singleton1.showMessage();

        // Create the second instance
        SingletonExample singleton2 = SingletonExample.getInstance();
        singleton2.showMessage();
        
        //added code-review
        if (singleton1 == singleton2) {
            System.out.println("Both instances refer to the same object.");
        } else {
            System.out.println("Instances are different.");
        }
        
        // Check if both instances have the same hash code
        if (singleton1.hashCode() == singleton2.hashCode()) {//32bit signed integer
            System.out.println("Both instances have the same hash code.");
        } else {
            System.out.println("Instances have different hash codes.");
        }
    }
}
