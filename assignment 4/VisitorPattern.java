// Element interface
interface Animal {
    void accept(AnimalVisitor visitor);
}

// Concrete elements
class Cheetah implements Animal {
    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }
}

class Tiger implements Animal {
    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }
}

class Leopard implements Animal {
    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }
}

// Visitor interface
interface AnimalVisitor {
    void visit(Cheetah cheetah);

    void visit(Tiger tiger);

    void visit(Leopard leopard);
}

// visitor 1
class FeedVisitor implements AnimalVisitor {
    @Override
    public void visit(Cheetah cheetah) {
        System.out.println("Feeding food to cheetah");
    }

    @Override
    public void visit(Tiger tiger) {
        System.out.println("Feeding food to tiger");
    }

    @Override
    public void visit(Leopard leopard) {
        System.out.println("Feeding food to leopard");
    }
}

// visitor 2
class CleanVisitor implements AnimalVisitor {
    @Override
    public void visit(Cheetah cheetah) {
        System.out.println("Cleaning the cheetah's area");
    }

    @Override
    public void visit(Tiger tiger) {
        System.out.println("Cleaning the tiger's area");
    }

    @Override
    public void visit(Leopard leopard) {
        System.out.println("Cleaning the leopard's area");
    }
}


public class VisitorPattern {
    public static void main(String[] args) {
        // Create different things that we need the visitor to visit
        Animal cheetah = new Cheetah();
        Animal tiger = new Tiger();
        Animal leopard = new Leopard();

        // Create visitors
        AnimalVisitor feedVisitor = new FeedVisitor();
        AnimalVisitor cleanVisitor = new CleanVisitor();

        //implementing feed visitor
        cheetah.accept(feedVisitor);
        tiger.accept(feedVisitor);
        leopard.accept(feedVisitor);

        System.out.println();

        // implementing clean visitor 
        cheetah.accept(cleanVisitor);
        tiger.accept(cleanVisitor);
        leopard.accept(cleanVisitor);
    }
}
