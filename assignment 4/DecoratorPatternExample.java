interface Tea {
    String Type();
    double cost();
}


class BasicTea implements Tea {
    @Override
    public String Type() {
        return "Basic Tea";
    }

    @Override
    public double cost() {
        return 10.0;
    }
}


abstract class TeaDecorator implements Tea {
    protected Tea decoratedTea;

    public TeaDecorator(Tea decoratedTea) {
        this.decoratedTea = decoratedTea;
    }

    @Override
    public String Type() {
        return decoratedTea.Type();
    }

    @Override
    public double cost() {
        return decoratedTea.cost();
    }
}


class MilkDecorator extends TeaDecorator {
    public MilkDecorator(Tea decoratedTea) {
        super(decoratedTea);
    }

    @Override
    public String Type() {
        return super.Type() + ", with Milk";
    }

    @Override
    public double cost() {
        return super.cost() + 15; // Milk costs 
    }
}

class SugarDecorator extends TeaDecorator {
    public SugarDecorator(Tea decoratedTea) {
        super(decoratedTea);
    }

    @Override
    public String Type() {
        return super.Type() + ", with Sugar";
    }

    @Override
    public double cost() {
        return super.cost() + 5.0; // Sugar costs 0.5
    }
}


public class DecoratorPatternExample {
    public static void main(String[] args) {
        // Create a basic tea
        Tea basicTea = new BasicTea();
        System.out.println("Basic Tea: " + basicTea.Type() + ", Cost: " + basicTea.cost());

        // Decorate the tea with milk
        Tea milkTea = new MilkDecorator(basicTea);
        System.out.println("Milk Tea: " + milkTea.Type() + ", Cost: " + milkTea.cost());

        // Decorate the tea with sugar
        Tea sweetTea = new SugarDecorator(basicTea);
        System.out.println("Sweet Tea: " + sweetTea.Type() + ", Cost: " + sweetTea.cost());

        // Decorate the tea with both milk and sugar
        Tea milkAndSugarTea = new SugarDecorator(new MilkDecorator(basicTea));
        System.out.println("Milk and Sugar Tea: " + milkAndSugarTea.Type() + ", Cost: " + milkAndSugarTea.cost());
    }
}
