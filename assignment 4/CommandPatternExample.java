public class CommandPatternExample {
    public static void main(String[] args) {
        ElectricalService electricalService = new ElectricalService();
        PaintingService paintingService = new PaintingService();

        // Dependencies are injected in commands
        Command electricalCommand = new ElectricalServiceRequestCommand(electricalService);
        Command paintingCommand = new PaintingServiceRequestCommand(paintingService);

        BuildingMaintenanceOffice maintenanceOffice = new BuildingMaintenanceOffice();
        maintenanceOffice.setCommand(1, electricalCommand);
        maintenanceOffice.setCommand(2, paintingCommand);

        maintenanceOffice.executeCommand(2);
    }
}

class ElectricalService {
    public void fixWiring() {
        System.out.println("Electrical service: Fixing wiring issue.");
    }

    public void installSocket() {
        System.out.println("Electrical service: Installing new socket.");
    }
}

class PaintingService {
    public void paintBuilding() {
        System.out.println("Painting service: Painting the building exterior.");
    }

    public void touchUpInterior() {
        System.out.println("Painting service: Touching up the interior walls.");
    }
}

abstract class Command {
    public abstract void execute();
}

class ElectricalServiceRequestCommand extends Command {
    private ElectricalService electricalService;

    public ElectricalServiceRequestCommand(ElectricalService electricalService) {
        this.electricalService = electricalService;
    }

    @Override
    public void execute() {
        System.out.println("Processing electrical service request...");
        electricalService.fixWiring();
        electricalService.installSocket();
        System.out.println("Electrical service request completed.");
    }
}

class PaintingServiceRequestCommand extends Command {
    private PaintingService paintingService;

    public PaintingServiceRequestCommand(PaintingService paintingService) {
        this.paintingService = paintingService;
    }

    @Override
    public void execute() {
        System.out.println("Processing painting service request...");
        paintingService.paintBuilding();
        paintingService.touchUpInterior();
        System.out.println("Painting service request completed.");
    }
}

class BuildingMaintenanceOffice {
    Command c[] = new Command[5];

    public void setCommand(int slot, Command command) {
        c[slot] = command;
    }

    public void executeCommand(int slot) {
        if (c[slot] != null) {
            c[slot].execute();
        } else {
            System.out.println("Invalid command slot.");
        }
    }
}
